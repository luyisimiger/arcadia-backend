# arcadia-backend

arcadia-backend

# For run project

python3 run.py

# For update database structure

python3 manage.py db migrate
python3 manage.py db upgrade

# For update database data

python3 manage.py sync entidades --db

python3 manage.py sync departamentos --db
python3 manage.py sync municipios --db
python3 manage.py sync pagadores --db
python3 manage.py sync prestadores --db
python3 manage.py sync personas