import datetime, random
# import env

from project.server.application import app
from project.server.models import *
from project.server.extensions import db

personas = Persona.query.all()
prestadores = PrestadorSalud.query.all()
pagadores = Pagador.query.all()


def get_persona():
    return random.choice(personas)

def get_prestador():
    return random.choice(prestadores)

def get_pagador():
    return random.choice(pagadores)
