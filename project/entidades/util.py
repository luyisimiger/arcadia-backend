
import csv
from os import path, mkdir
ACTUALDIR = path.abspath(path.dirname(__file__))
JSON_SERVER_FILE = path.join(ACTUALDIR, 'entidades.json')


def get_cvsDictReader(FILE_NAME):
    """docs for get_cvsfile function"""
    fullfilename = path.join(ACTUALDIR, 'archivos', FILE_NAME)
    csv_reader = csv.DictReader(open(fullfilename, encoding='utf8'))
    return csv_reader


def get_jsonfile(mode):
    jsfile = None
    try:
        jsfile = open(JSON_SERVER_FILE, mode=mode, encoding='utf8')
    except FileNotFoundError:
        mkdir(JSON_SERVER_FILE)
        jsfile = open(JSON_SERVER_FILE, mode=mode, encoding='utf8')

    return jsfile


def syncjson_entity(entityname, FILE_NAME, func_process_row):
    """docs for entityname's syncjson function"""

    # get csv reader
    csv_reader = get_cvsDictReader(FILE_NAME)

    # get object list
    object_list = []
    for row in csv_reader:
        _object = func_process_row(row)
        object_list.append(_object)

    # sync object list with json file
    sync_jsonfile_entity(entityname, object_list)


def sync_jsonfile_entity(entityname, object_list):

    import json

    data = dict()

    jsfile = get_jsonfile(mode='r')

    try:
        data = json.load(jsfile)
    except json.decoder.JSONDecodeError:
        pass
    finally:
        jsfile.close

    data[entityname] = object_list
    jsfile = get_jsonfile(mode='w')
    json.dump(data, jsfile, indent=4)
    jsfile.close


def syncjson_entity_excel(entityname, sheet, func_process_row):
    """docs for entityname's from excel syncjson function"""

    # get object list
    object_list = []
    for r in range(2, 1 + sheet.max_row):
        _object = func_process_row(sheet, r)

        if _object is not None:
            object_list.append(_object)

    # sync object list with json file
    sync_jsonfile_entity(entityname, object_list)


def update_object_from_dict(_object, data):
    for attr, value in data.items():
        setattr(_object, attr, value)
