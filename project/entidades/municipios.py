
from os import path
from project.server.extensions import db
from project.server.models import Municipio, Departamento
from . import util
ACTUALDIR = path.abspath(path.dirname(__file__))
FILE_NAME = 'MinSalud_Divipola_-_Municipios.csv'


def process_row(row):
    return {
        'codigo_departamento': row['iddepto'],
        'codigo': row['idmupio'],
        'nombre': row['nommpio'].strip(),
        'latitud': float(row['mpiolatitud']),
        'longitud': float(row['mpiolongitud'])
    }


def sync(db=False):
    """docs for Municipio sync function"""
    if db:
        syncdb()
    else:
        syncjson()


def syncdb():
    """docs for Municipio syncdb function"""
    csv_reader = util.get_cvsDictReader(FILE_NAME)
    for row in csv_reader:
        codigo_departamento = row['iddepto']
        codigo = row['idmupio']
        nombre = row['nommpio'].strip()
        latitud = float(row['mpiolatitud'])
        longitud = float(row['mpiolongitud'])
        _object = (Municipio.query.filter_by(codigo=codigo)).first()
        departamento = (Departamento.query.filter_by(
            codigo=codigo_departamento)).first()

        if _object is None:
            _object = Municipio(codigo=codigo,
                                nombre=nombre,
                                latitud=latitud,
                                longitud=longitud,
                                departamento=departamento)
            db.session.add(_object)
        else:
            _object.nombre = nombre
            _object.latitud = latitud
            _object.longitud = longitud
            _object.codigo_departamento = codigo_departamento
        db.session.commit()


def syncjson():
    """docs for Municipio syncjson function"""

    # sync json entity
    util.syncjson_entity("municipios", FILE_NAME, process_row)
