import logging
from project.server.extensions import db
from project.server.models import Pagador
from project.server.models.enum import TipoPagadorSalud
from . import util, entidadessalud as ent

OBJECTS_TO_SYNC = 100

_PAGADORES = [
    (TipoPagadorSalud.DIR_TERRITORIAL_SALUD, "Dir. Territoriales de Salud"),
    (TipoPagadorSalud.ARL, "ARP"),
    (TipoPagadorSalud.ENTIDAD_ADAPTADA, "Entidades Adaptadas"),
    (TipoPagadorSalud.EPS, "EPS Contributivo"),
    (TipoPagadorSalud.EPS, "EPS Subsidiado")
]


def gettipo(text):
    for t in _PAGADORES:
        if text == t[1]:
            return t[0]
    return None


def esPagador(tipo):
    return True if gettipo(tipo) is not None else False


def process_row(sheet, r):

    tipo = gettipo((sheet.cell(row=r, column=1)).value)

    # row is not pagador
    if tipo is None:
        return None

    _object = ent.process_row(sheet, r)
    del _object["departamento"]
    del _object["municipio"]
    _object['tipo'] = tipo
    return _object


def sync(db=False):
    """docs for Pagador sync function"""
    if db:
        syncdb()
    else:
        syncjson()


def process_pagador(sheet, r):

    _row = process_row(sheet, r)

    # si el registro no es pagador
    if _row is None:
        return None

    codigo = _row["codigo"]
    _object = (Pagador.query.filter_by(codigo=codigo)).first()

    if _object is None:
        _object = Pagador(**_row)
        db.session.add(_object)

    else:
        util.update_object_from_dict(_object, _row)

    return _object


def syncdb():
    """docs for Pagador syncdb function"""

    logging.info("reading file...")
    sheet = ent.read_sheet()
    logging.info("reading file...OK")

    for r in range(2, 1 + sheet.max_row):

        _object = process_pagador(sheet, r)

        # si el registro no es pagador
        if _object is None:
            continue

        logging.info(_object)

        # syncromize to database
        if r % OBJECTS_TO_SYNC == 0:
            db.session.commit()

    # syncromize to database
    db.session.commit()


def syncjson():
    """docs for Pagador syncjson function"""

    # get sheet data
    sheet = ent.read_sheet()

    # sync json entity
    util.syncjson_entity_excel("pagadores", sheet, process_row)
