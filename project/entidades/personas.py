import logging

from datetime import datetime
from os import path
from project.server.extensions import db
from project.server.models import Persona
from . import util
ACTUALDIR = path.abspath(path.dirname(__file__))
FILE_NAME = 'PERSONAS.csv'


def process_row(_row):

    # dia_, mes_, anio_ = _row["FECHA_NAC"].split("/")

    # anio = int(anio_)
    # mes = int(mes_)
    # dia = int(dia_)

    # mesx = mes
    # mes = dia if mesx > 12 else mesx
    # dia = dia if mesx < 12 else mesx

    return {
        "apellido1": _row["1ER_APELLIDO"],
        "apellido2": _row["2DO_APELLIDO"],
        "nombre1": _row["1ER_NOMBRE"],
        "nombre2": _row["2DO_NOMBRE"],
        "tipoIdentificacion": _row["TIPO DOCUMENTO"],
        "numeroIdentificacion": _row["NUMERO DOCUMENTO"].replace(".", ""),
        "fechaNacimiento": datetime(1990, 1, 1),
        "sexo": "M"
    }


def sync(db=False):
    """docs for Persona sync function"""
    if db:
        syncdb()
    else:
        syncjson()


def syncdb():
    """docs for Persona syncdb function"""

    csv_reader = util.get_cvsDictReader(FILE_NAME)
    for row in csv_reader:

        _dict = process_row(row)

        # search existing object
        # logging.info("search existing object")
        _object = (Persona.query.filter_by(
                 tipoIdentificacion=_dict["tipoIdentificacion"],
                 numeroIdentificacion=_dict["numeroIdentificacion"])).first()

        # add object
        if _object is None:
            # logging.info("add object")
            _object = Persona(
                    apellido1=_dict["apellido1"],
                    apellido2=_dict["apellido2"],
                    nombre1=_dict["nombre1"],
                    nombre2=_dict["nombre2"],
                    tipoIdentificacion=_dict["tipoIdentificacion"],
                    numeroIdentificacion=_dict["numeroIdentificacion"],
                    fechaNacimiento=_dict["fechaNacimiento"],
                    sexo=_dict["sexo"])

            db.session.add(_object)

        # edit object
        else:
            # logging.info("edit object")
            _object.apellido1 = _dict["apellido1"]
            _object.apellido2 = _dict["apellido2"]
            _object.nombre1 = _dict["nombre1"]
            _object.nombre2 = _dict["nombre2"]
            _object.tipoIdentificacion = _dict["tipoIdentificacion"]
            _object.numeroIdentificacion = _dict["numeroIdentificacion"]
            _object.fechaNacimiento = _dict["fechaNacimiento"]

        # save object in database
        # logging.info("save object in database")
        db.session.commit()


def syncjson():
    """docs for Persona syncjson function"""

    # sync json entity
    util.syncjson_entity("personas", FILE_NAME, process_row)
