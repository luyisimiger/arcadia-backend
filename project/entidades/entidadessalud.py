import logging
import openpyxl
from os import path
from .prestadores import (esPrestador, process_prestador)
from .pagadores import (process_pagador)
from project.server.extensions import db
from project.server.models.enum import TipoEntidadSalud

FILE_NAME = 'Entidades.xlsx'
SHEET_NAME = 'Entidades'
ACTUALDIR = path.abspath(path.dirname(__file__))
OBJECTS_TO_SYNC = 10


def read_sheet():
    filepath = path.join(ACTUALDIR, 'archivos', FILE_NAME)
    workbook = openpyxl.load_workbook(filepath)
    sheet = workbook[SHEET_NAME]
    return sheet


def get_clase(sheet, r):

    tipo = (sheet.cell(row=r, column=1)).value
    return TipoEntidadSalud.PRESTADOR \
        if esPrestador(tipo) else TipoEntidadSalud.PAGADOR


def process_row(sheet, r):
    
    d = {
        'codigo': (sheet.cell(row=r, column=2)).value,
        'sigla': (sheet.cell(row=r, column=3)).value,
        'nombre': (sheet.cell(row=r, column=4)).value,
        'departamento': (sheet.cell(row=r, column=6)).value,
        'municipio': (sheet.cell(row=r, column=7)).value,
        'direccion': (sheet.cell(row=r, column=8)).value,
        'telefono': (sheet.cell(row=r, column=9)).value,
        'url': (sheet.cell(row=r, column=11)).value
    }

    for k, v in d.items():
        d[k] = "" if v is None else v

    return d


def sync(db=False):
    """docs for PrestadorSalud sync function"""
    # if db:
    syncdb()
    # else:
    #    syncjson()


def syncdb():
    """docs for PrestadorSalud syncdb function"""

    logging.info("reading file...")
    sheet = read_sheet()
    logging.info("reading file...OK")

    rowini = 112186  # 2
    rows = rowini + sheet.max_row
    for r in range(rowini, rows):

        clase = get_clase(sheet, r)
        _object = None

        if clase == TipoEntidadSalud.PAGADOR:
            _object = process_pagador(sheet, r)
        elif clase == TipoEntidadSalud.PRESTADOR:
            _object = process_prestador(sheet, r)

        logging.info("(%s / %s) %s" % (r, rows, _object))

        # syncromize to database
        if r % OBJECTS_TO_SYNC == 0:
            db.session.commit()

    db.session.commit()
