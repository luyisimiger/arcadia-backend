
from os import path
from project.server.extensions import db
from project.server.models import Departamento
from . import util
ACTUALDIR = path.abspath(path.dirname(__file__))
FILE_NAME = 'MinSalud_Divipola_-_Departamentos.csv'


def process_row(row):
    return {
        'codigo': row['iddepto'],
        'nombre': row['nomdepto'].strip(),
        'latitud': float(row['deptolatitud']),
        'longitud': float(row['deptolongitud'])
    }


def sync(db=False):
    """docs for Departamento sync function"""
    if db:
        syncdb()
    else:
        syncjson()


def syncdb():
    """docs for Departamento syncdb function"""
    csv_reader = util.get_cvsDictReader(FILE_NAME)
    for row in csv_reader:
        codigo = row['iddepto']
        nombre = row['nomdepto'].strip()
        latitud = float(row['deptolatitud'])
        longitud = float(row['deptolongitud'])
        _object = (Departamento.query.filter_by(codigo=codigo)).first()
        if _object is None:
            _object = Departamento(codigo=codigo,
                                   nombre=nombre,
                                   latitud=latitud,
                                   longitud=longitud)
            db.session.add(_object)
        else:
            _object.nombre = nombre
            _object.latitud = latitud
            _object.longitud = longitud
        db.session.commit()


def syncjson():
    """docs for Departamento syncjson function"""

    # sync json entity
    util.syncjson_entity("departamentos", FILE_NAME, process_row)
