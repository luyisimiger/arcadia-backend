import logging
from project.server.extensions import db
from project.server.models import PrestadorSalud, Municipio, Departamento
from project.server.models.enum import TipoPrestadorSalud
from . import util, entidadessalud as ent

OBJECTS_TO_SYNC = 10

_PRESTADORES = [
    (TipoPrestadorSalud.IPS, "IPS Privadas"),
    (TipoPrestadorSalud.IPS, "IPS Públicas"),
    # (TipoPrestadorSalud.PROFESIONAL_INDEPENDIENTE, "Profesional Independiente"),
    # (TipoPrestadorSalud.TRANSPORTE_ESPECIAL, "Transporte Especial")
]


def gettipo(text):
    for t in _PRESTADORES:
        if text == t[1]:
            return t[0]
    return None


def esPrestador(tipo):
    return True if gettipo(tipo) is not None else False


def process_row(sheet, r):

    tipo = gettipo((sheet.cell(row=r, column=1)).value)

    # row is not prestador
    if tipo is None or sheet.cell(row=r, column=6).value not in ("Bolívar", "Sucre"):
        return None

    _object = ent.process_row(sheet, r)
    _object['tipo'] = tipo
    return _object


def process_prestador(sheet, r):
    _row = process_row(sheet, r)

    # si el registro no es prestador
    if _row is None:
        return None

    codigo = _row["codigo"]
    departamento = _row["departamento"]
    municipio = _row["municipio"]

    _object = (PrestadorSalud.query.filter_by(codigo=codigo)).first()
    _obj_municipio = Municipio.query.join(Departamento).filter(Departamento.nombre == departamento, Municipio.nombre.ilike(municipio)).first()

    _row["municipio"] = _obj_municipio

    if _object is None:
        _object = PrestadorSalud(**_row)
        db.session.add(_object)

    else:
        util.update_object_from_dict(_object, _row)

    return _object


def sync(db=False):
    """docs for PrestadorSalud sync function"""
    if db:
        syncdb()
    else:
        syncjson()


def syncdb():
    """docs for PrestadorSalud syncdb function"""

    logging.info("reading file...")
    sheet = ent.read_sheet()
    logging.info("reading file...OK")

    for r in range(2, 1 + sheet.max_row):
        _object = process_prestador(sheet, r)

        # si el registro no es prestador
        if _object is None:
            continue

        logging.info(_object)

        # syncromize to database
        if r % OBJECTS_TO_SYNC == 0:
            db.session.commit()

    db.session.commit()


def syncjson():
    """docs for PrestadorSalud syncjson function"""

    # get sheet data
    sheet = ent.read_sheet()

    # sync json entity
    util.syncjson_entity_excel("prestadores", sheet, process_row)
