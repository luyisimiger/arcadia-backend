"docs for sync module"

import logging

from flask_script import Manager

from . import departamentos as _departamentos
from . import municipios as _municipios
from . import entidadessalud as _entidadessalud
from . import pagadores as _pagadores
from . import prestadores as _prestadores
from . import personas as _personas

manager = Manager(usage='Perform json server file or database operations')
logging.basicConfig(format='%(asctime)s - %(message)s', level=logging.INFO)


@manager.command
def departamentos(db=False):
    """sync your Departamento's entities
    with json server file or database"""

    logging.info("sync your Departamento's entities...")
    _departamentos.sync(db)
    logging.info("sync process finish")


@manager.command
def municipios(db=False):
    """sync your Municipio's entities
    with json server file or database"""

    logging.info("sync your Municipio's entities...")
    _municipios.sync(db)
    logging.info("sync process finish")


@manager.command
def entidadessalud(db=False):
    """sync your EntidadSalud's entities
    with json server file or database"""
    logging.info("sync your EntidadSalud's entities...")
    _entidadessalud.sync(db)
    logging.info("sync process finish")


@manager.command
def pagadores(db=False):
    """sync your Pagador's entities
    with json server file or database"""
    logging.info("sync your Pagador's entities...")
    _pagadores.sync(db)
    logging.info("sync process finish")


@manager.command
def prestadores(db=False):
    """sync your Pagador's entities
    with json server file or database"""
    logging.info("sync your Prestador's entities...")
    _prestadores.sync(db)
    logging.info("sync process finish")


@manager.command
def personas(db=False):
    """sync your Persona's entities
    with json server file or database"""
    logging.info("sync your Persona's entities...")
    _personas.sync(db)
    logging.info("sync process finish")


@manager.command
def entidades(db=False):
    """sync your all entities
    with json server file or database"""

    logging.info("sync your entities...")

    departamentos(db)
    municipios(db)
    pagadores(db)
    prestadores(db)
    personas(db)

    logging.info("sync process finish")
