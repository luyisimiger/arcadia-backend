from flask_script import Command, Manager, Option, prompt

from project.server.api import remisiones as logic

manager = Manager(usage="Ejecutar operaciones sobre remisiones")

class Presentar(Command):
    "command for presentar una remision"
    
    option_list = (
        Option('--id', '-n', dest='id'),
    )

    # def run(self, nombre, url, url_image):
    def run(self, id):        
        logic.presentar(id)

# register commands
manager.add_command('presentar', Presentar())
