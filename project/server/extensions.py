from flask_bcrypt import Bcrypt
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
from flask_mail import Mail


db = SQLAlchemy()
bcrypt = Bcrypt()
ma = Marshmallow()
mail = Mail()
