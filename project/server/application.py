
import os
import connexion
from flask_cors import CORS


dirpath = os.path.abspath(os.path.dirname(__file__))
connex_app = connexion.FlaskApp(__name__, specification_dir=dirpath)
app = connex_app.app


app_settings = os.getenv('APP_SETTINGS',
                         'project.server.config.DevelopmentConfig')
app.config.from_object(app_settings)

cors = CORS(app)

connex_app.add_api('swagger/swagger.yml')


@app.route('/')
def hello():
    return 'Hello World, arcadia project!'
