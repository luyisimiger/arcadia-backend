"""
This is the people module and supports all the REST actions for the
people data
"""

from datetime import datetime
from flask import make_response, abort

from project.server.extensions import db
from project.server.data.modelos.Persona import Persona
from ..schemas import PersonaSchema


def load(**kwpersona):
    fechaNacimiento = kwpersona.get("fechaNacimiento", "")
    kwpersona["nombre1"] = kwpersona.get("nombre1", "")
    kwpersona["nombre2"] = kwpersona.get("nombre2", "")
    kwpersona["apellido1"] = kwpersona.get("apellido1", "")
    kwpersona["apellido2"] = kwpersona.get("apellido2", "")
    kwpersona["fechaNacimiento"] = datetime.fromisoformat(fechaNacimiento) if fechaNacimiento != "" else datetime(1900, 1, 1)
    kwpersona["sexo"] = kwpersona.get("sexo", "M")

    return Persona(**kwpersona)


def create(person):
    """
    This function creates a new person in the people structure
    based on the passed in person data

    :param person:  person to create in people structure
    :return:        201 on success, 406 on person exists
    """

    nombre1 = person.get("nombre1")
    nombre2 = person.get("nombre2")
    tipoIdentificacion = person.get("tipoIdentificacion")
    numeroIdentificacion = person.get("numeroIdentificacion")

    existing_instance = (
        Persona.query.filter(Persona.tipoIdentificacion == tipoIdentificacion)
        .filter(Persona.numeroIdentificacion == numeroIdentificacion)
        .one_or_none()
    )

    # Can we insert this person?
    if existing_instance is None:

        # Create a person instance using the schema and the passed in person
        schema = PersonaSchema()
        new_instance = schema.make_instance(person)

        # Add the person to the database
        db.session.add(new_instance)
        db.session.commit()

        # Serialize and return the newly created person in the response
        data = schema.dump(new_instance)

        return data, 201

    # Otherwise, nope, person exists already
    else:
        abort(
            409,
            "La Persona {nombre1} {nombre2} exists already".format(
                nombre1=nombre1, nombre2=nombre2
            ),
        )


def read_all(q=""):
    """
    This function responds to a request for /api/people
    with the complete lists of people

    :return:        json string of list of people
    """

    exp = "%" + q + "%"

    # Create the list of people from our data
    all_instances = Persona.query \
        .filter( (Persona.nombre1.ilike(exp) | Persona.nombre2.ilike(exp) | Persona.apellido1.ilike(exp) | Persona.apellido2.ilike(exp)) ) \
        .order_by(Persona.apellido1) \
        .all()

    # Serialize the data for the response
    schema = PersonaSchema(many=True)
    data = schema.dump(all_instances)

    return data


def read_one(id):
    """
    This function responds to a request for /api/people/{person_id}
    with one matching person from people

    :param person_id:   Id of person to find
    :return:            person matching id
    """

    # Get the person requested
    existing_instance = Persona.query.filter(Persona.id == id).one_or_none()

    # Did we find a person?
    if existing_instance is not None:

        # Serialize the data for the response
        schema = PersonaSchema()
        data = schema.dump(existing_instance)
        return data

    # Otherwise, nope, didn't find that person
    else:
        abort(
            404,
            "Persona not found for Id: {id}".format(id=id),
        )


def update(id, person):
    """
    This function updates an existing person in the people structure

    :param person_id:   Id of the person to update in the people structure
    :param person:      person to update
    :return:            updated person structure
    """

    # Get the person requested from the db into session
    existing_instance = Persona.query.filter(
        Persona.id == id
    ).one_or_none()

    # Did we find a person?
    if existing_instance is not None:

        # turn the passed in person into a db object
        schema = PersonaSchema()
        update = schema.make_instance(person)

        # Set the id to the person we want to update
        update.id = existing_instance.id

        # merge the new object into the old and commit it to the db
        db.session.merge(update)
        db.session.commit()

        # return updated person in the response
        data = schema.dump(existing_instance)

        return data, 200

    # Otherwise, nope, didn't find that person
    else:
        abort(
            404,
            "Persona not found for Id: {id}".format(id=id),
        )


def delete(id):
    """
    This function deletes a person from the people structure

    :param person_id:   Id of the person to delete
    :return:            200 on successful delete, 404 if not found
    """

    # Get the person requested
    existing_instance = Persona.query.filter(Persona.id == id).one_or_none()

    # Did we find a person?
    if existing_instance is not None:

        db.session.delete(existing_instance)
        db.session.commit()
        return make_response(
            "Persona {id} deleted".format(id=id), 200
        )

    # Otherwise, nope, didn't find that person
    else:
        abort(
            404,
            "Persona not found for Id: {id}".format(id=id),
        )
