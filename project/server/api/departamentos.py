"""
This is the people module and supports all the REST actions for the
people data
"""

from flask import abort

from project.server.data.entidades import Departamento
from project.server.extensions import db
from project.server.schemas import DepartamentoSchema


def create(item):
    pass


def read_all(q=""):
    """
    This function responds to a request for /api/people
    with the complete lists of people

    :return:        json string of list of people
    """

    # Create the list of people from our data
    all_instances = Departamento.query.filter(Departamento.nombre.ilike("%"+q+"%")).order_by(Departamento.codigo).all()

    # Serialize the data for the response
    schema = DepartamentoSchema(many=True)
    data = schema.dump(all_instances)

    return data


def read_one(id):
    """
    This function responds to a request for /api/people/{person_id}
    with one matching person from people

    :param person_id:   Id of person to find
    :return:            person matching id
    """

    # Get the person requested
    existing_instance = Departamento.query \
        .filter(Departamento.id == id).one_or_none()

    # Did we find a person?
    if existing_instance is not None:

        # Serialize the data for the response
        schema = DepartamentoSchema()
        data = schema.dump(existing_instance)
        return data

    # Otherwise, nope, didn't find that person
    else:
        abort(
            404,
            "Departamento not found for Id: {id}".format(id=id),
        )


def update(id, item):
    """
    This function updates an existing person in the people structure

    :param person_id:   Id of the person to update in the people structure
    :param person:      person to update
    :return:            updated person structure
    """

    # Get the person requested from the db into session
    existing_instance = Departamento.query.filter(
        Departamento.id == id
    ).one_or_none()

    # Did we find a person?
    if existing_instance is not None:

        # turn the passed in person into a db object
        schema = DepartamentoSchema()
        update = schema.make_instance(item)

        # Set the id to the person we want to update
        update.id = existing_instance.id

        # merge the new object into the old and commit it to the db
        db.session.merge(update)
        db.session.commit()

        # return updated person in the response
        data = schema.dump(existing_instance)

        return data, 200

    # Otherwise, nope, didn't find that person
    else:
        abort(
            404,
            "Departamento not found for Id: {id}".format(id=id),
        )


def delete(id):
    pass
