# project/server/api/__init_.py
from flask import Blueprint

api_blueprint = Blueprint('api', __name__, url_prefix='/api')


from .views import remision, prestador, pagador
