"""
This is the people module and supports all the REST actions for the
people data
"""

from flask import abort

from project.server.data import PrestadorSalud, Municipio, Departamento
from ..servicios import prestadores as servicio_prestador
from ..schemas import PrestadorSaludSchema



def _serialize(instances, many=False):
    schema = PrestadorSaludSchema(many=True)
    data = schema.dump(instances)
    return data


def serialize_one(existing_instance):
    return _serialize(existing_instance)


def serialize_many(all_instances):
    return _serialize(all_instances, many=True)


def load(**kwargs):
    id = kwargs.get("id", None)
    prestador = servicio_prestador.read_one(id)

    if prestador is not None:
        return prestador
    
    # return PrestadorSalud(**kwargs)
    return None


def create(item):
    pass


def read_all(q="", m="", d=""):
    """
    This function responds to a request for /api/people
    with the complete lists of people

    :return:        json string of list of people
    """

    # Create the list of people from our data
    all_instances = PrestadorSalud.query \
        .join(PrestadorSalud.municipio) \
        .join(Municipio.departamento) \
        .filter(PrestadorSalud.nombre.ilike("%" + q + "%")) \
        .filter(Municipio.nombre.ilike("%" + m + "%")) \
        .filter(Departamento.nombre.ilike("%" + d + "%")) \
        .order_by(PrestadorSalud.codigo) \
        .all()

    # Serialize the data for the response
    return serialize_many(all_instances)


def read_one(id):
    """
    This function responds to a request for /api/people/{person_id}
    with one matching person from people

    :param person_id:   Id of person to find
    :return:            person matching id
    """

    # Get the person requested
    existing_instance = PrestadorSalud.query \
        .filter(PrestadorSalud.id == id).one_or_none()

    # Did we find a person?
    if existing_instance is not None:

        # Serialize the data for the response
        schema = PrestadorSaludSchema()
        data = schema.dump(existing_instance).data
        return data

    # Otherwise, nope, didn't find that person
    else:
        abort(
            404,
            "PrestadorSalud not found for Id: {id}".format(id=id),
        )


def seleccionar(**kwargs):
    
    # Create the list of instances from our data
    instances = servicio_prestador.seleccionar(**kwargs)

    # Serialize the data for the response
    return serialize_many(instances)


def update(id, item):
    pass


def delete(id):
    pass
