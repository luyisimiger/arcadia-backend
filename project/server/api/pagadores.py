"""
This is the people module and supports all the REST actions for the
people data
"""

from flask import abort

from project.server.data.entidades import Pagador
from ..servicios import pagadores as servicio_pagador
from ..schemas import PagadorSchema


def _serialize(instances, many=False):
    schema = PagadorSchema(many=True)
    data = schema.dump(instances)
    return data


def serialize_one(existing_instance):
    return _serialize(existing_instance)

def serialize_many(all_instances):
    return _serialize(all_instances, many=True)

def load(**kwargs):
    id = kwargs.get("id", None)
    pagador = servicio_pagador.read_one(id)

    if pagador is not None:
        return pagador
    
    # return PrestadorSalud(**kwargs)
    return None

def create(item):
    pass


def read_all(q=""):
    """
    This function responds to a request for /api/people
    with the complete lists of people

    :return:        json string of list of people
    """

    # Create the list of people from our data
    all_instances = Pagador.query \
        .filter(Pagador.nombre.ilike("%" + q + "%")) \
        .order_by(Pagador.codigo).all()

    # Serialize the data for the response
    schema = PagadorSchema(many=True)
    data = schema.dump(all_instances)

    return data


def read_one(id):
    """
    This function responds to a request for /api/people/{person_id}
    with one matching person from people

    :param person_id:   Id of person to find
    :return:            person matching id
    """

    # Get the person requested
    existing_instance = Pagador.query \
        .filter(Pagador.id == id).one_or_none()

    # Did we find a person?
    if existing_instance is not None:

        # Serialize the data for the response
        schema = PagadorSchema()
        data = schema.dump(existing_instance)
        return data

    # Otherwise, nope, didn't find that person
    else:
        abort(
            404,
            "Pagador not found for Id: {id}".format(id=id),
        )

def seleccionar(**kwargs):
    
    # Create the list of instances from our data
    instances = servicio_pagador.seleccionar(**kwargs)

    # Serialize the data for the response
    return serialize_many(instances)

def update(id, item):
    pass


def delete(id):
    pass
