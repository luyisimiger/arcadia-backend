from flask import Blueprint, make_response, jsonify, request

from .. import api_blueprint
from .. import remisiones as logic


@api_blueprint.route('/remisiones/fake')
def remision_create_fake():
    instance = logic.create_fake()
    return make_response(jsonify(instance)), 200


@api_blueprint.route('/remisiones')
def remision_fetch():
    instances = logic.read_all()
    return make_response(jsonify(instances)), 200


@api_blueprint.route('/remisiones/actualizar/bitacora', methods=['POST'])
def remision_actualizar_bitacora():
    data = request.get_json()
    idremision = data['idremision']
    descripcion = data['descripcion']
    instance = logic.actualizar_bitacora(idremision, descripcion)
    return make_response(jsonify(instance)), 200


@api_blueprint.route('/remisiones/presentar', methods=['POST'])
def remision_presentar():
    data = request.get_json()
    idremision = data['idremision']
    idprestadores = data['idprestadores']
    instance = logic.presentar(idremision, idprestadores)
    return make_response(jsonify(instance)), 200


@api_blueprint.route('/remisiones/solicitar', methods=['POST'])
def remision_solicitar():
    data = request.get_json()
    kwremision = data['remision']
    instance = logic.solicitar(kwremision)
    return make_response(jsonify(instance)), 200


@api_blueprint.route('/remisiones/aceptar', methods=['POST'])
def remision_aceptar():
    data = request.get_json()
    idremision = data['idremision']
    idprestador = data['idprestador']
    instance = logic.aceptar(idremision, idprestador)
    return make_response(jsonify(instance)), 200


@api_blueprint.route('/remisiones/aceptar/multiple', methods=['POST'])
def remision_aceptar_multiple():
    data = request.get_json()
    idremision = data['idremision']
    idprestadores = data['idprestadores']
    
    for idp in idprestadores:
        instance = logic.aceptar(idremision, idp)

    return make_response(jsonify(instance)), 200


@api_blueprint.route('/remisiones/recibir', methods=['POST'])
def remision_recibir():
    data = request.get_json()
    idremision = data['idremision']
    idprestador = data['idprestador']
    instance = logic.recibir(idremision, idprestador)
    return make_response(jsonify(instance)), 200


@api_blueprint.route('/remisiones/<int:id>')
def remision_one(id):
    instance = logic.read_one(id)
    return make_response(jsonify(instance)), 200


@api_blueprint.route('/remisiones/<int:id>/recepcionar')
def remision_recepcionar(id):
    instance = logic.recepcionar(id)
    return make_response(jsonify(instance)), 200


@api_blueprint.route('/remisiones/<int:id>/presentar')
def remision_presentar_id(id):
    instance = logic.presentar_id(id)
    return make_response(jsonify(instance)), 200


@api_blueprint.route('/remisiones/<int:id>/aceptar')
def remision_aceptar_id(id):
    instance = logic.aceptar_id(id)
    return make_response(jsonify(instance)), 200


@api_blueprint.route('/remisiones/<int:id>/recibir')
def remision_recibir_id(id):
    instance = logic.recibir_id(id)
    return make_response(jsonify(instance)), 200
