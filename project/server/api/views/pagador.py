from flask import Blueprint, make_response, jsonify, request

from .. import api_blueprint
from .. import pagadores as logic


@api_blueprint.route('/pagadores/seleccionar', methods=['GET', 'POST'])
def pagador_seleccionar():
    data = request.get_json() or {}
    instance = logic.seleccionar(**data)
    return make_response(jsonify(instance)), 200
