from flask import Blueprint, make_response, jsonify, request

from .. import api_blueprint
from .. import prestadores as logic


@api_blueprint.route('/prestadores/seleccionar', methods=['GET', 'POST'])
def prestador_seleccionar():
    data = request.get_json()
    instance = logic.seleccionar(**data)
    return make_response(jsonify(instance)), 200
