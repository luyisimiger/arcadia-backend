"""
This is the people module and supports all the REST actions for the
people data
"""

from flask import abort

from project.server.data import Municipio, Departamento
from ..schemas import MunicipioSchema


def create(item):
    pass


def read_all(q="", d=""):
    """
    This function responds to a request for /api/people
    with the complete lists of people

    :return:        json string of list of people
    """

    # Create the list of people from our data
    all_instances = Municipio.query \
        .join(Municipio.departamento) \
        .filter(Municipio.nombre.ilike("%" + q + "%")) \
        .filter(Departamento.nombre.ilike("%" + d + "%")) \
        .order_by(Municipio.codigo).all()

    # Serialize the data for the response
    schema = MunicipioSchema(many=True)
    data = schema.dump(all_instances)

    return data


def read_one(id):
    """
    This function responds to a request for /api/people/{person_id}
    with one matching person from people

    :param person_id:   Id of person to find
    :return:            person matching id
    """

    # Get the person requested
    existing_instance = Municipio.query \
        .filter(Municipio.id == id).one_or_none()

    # Did we find a person?
    if existing_instance is not None:

        # Serialize the data for the response
        schema = MunicipioSchema()
        data = schema.dump(existing_instance)
        return data

    # Otherwise, nope, didn't find that person
    else:
        abort(
            404,
            "Municipio not found for Id: {id}".format(id=id),
        )


def update(id, item):
    pass


def delete(id):
    pass
