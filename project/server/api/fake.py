import datetime, random, uuid
import env
import lorem

from project.server.application import app
from project.server.data import Persona, Pagador, PrestadorSalud, PacienteInfo, AcudienteInfo, ProfesionalSaludInfo, RemisionEstado
from project.server.data.helpers import enum_values
from project.server.extensions import db


direcciones = ["Bellavista Mz Q Lote 3",
    "Bellavista Mz J Lote 5",
    "Escallon Villa Etapa 3"
]

servicios = [ "serv " + str(i) for i in range(0, 10)]

telefonos = [ "31052635264", "31256201425", "305104520125" ]

def fake_persona():
    personas = Persona.query.all()
    return random.choice(personas)

def fake_prestador():
    prestadores = PrestadorSalud.query.all()
    return random.choice(prestadores)

def fake_pagador():
    pagadores = Pagador.query.all()
    return random.choice(pagadores)

def fake_direccion():
    return random.choice(direcciones)

def fake_telefono():
    return random.choice(telefonos)

def fake_codigo():
    return uuid.uuid4().hex

def fake_estado():
    return random.choice(enum_values(RemisionEstado))

def fake_servicio_referente():
    return lorem.sentence()

def servicio_referencia():
    return lorem.sentence()

def fake_info_clinica():
    return lorem.sentence()

def fake_paciente():
    p = PacienteInfo(persona = fake_persona())
    p.direccion = fake_direccion()
    p.telefono = fake_telefono()
    return p

def fake_acudiente():
    p = AcudienteInfo(persona = fake_persona())
    p.direccion = fake_direccion()
    p.telefono = fake_telefono()
    return p

def fake_profesional():
    p = ProfesionalSaludInfo(persona = fake_persona())
    p.direccion = fake_direccion()
    p.telefono = fake_telefono()
    return p