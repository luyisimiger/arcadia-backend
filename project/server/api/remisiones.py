"""
This is the people module and supports all the REST actions for the
people data
"""

from flask import make_response, abort
from sqlalchemy.sql.expression import or_
import datetime

from ..extensions import db
from project.server.data import Remision, RemisionEstado, Persona, PrestadorSalud, PacienteInfo, AcudienteInfo, ProfesionalSaludInfo
from ..servicios import remisiones as servicio_remision, prestadores as servicio_prestador
from ..api import fake
from ..api.personas import load as load_persona
from ..api.prestadores import load as load_prestador
from ..api.pagadores import load as load_pagador
from ..schemas import RemisionSchema, PersonaSchema, PacienteInfoSchema, AcudienteInfoSchema, ProfesionalSaludInfoSchema


def _serialize(instances, many=False):
    schema = RemisionSchema(many=many)
    data = schema.dump(instances)
    return data


def serialize_one(existing_instance):
    return _serialize(existing_instance)


def serialize_many(all_instances):
    return _serialize(all_instances, many=True)


def read_one(id):
    """
    This function responds to a request for /api/people/{person_id}
    with one matching person from people

    :param person_id:   Id of person to find
    :return:            person matching id
    """

    # Get the person requested
    existing_instance = servicio_remision.read_one(id)

    if existing_instance is not None:
    
        # Serialize the data for the response
        return serialize_one(existing_instance)
    
    # Otherwise, nope, didn't find that person
    else:
        abort(
            404,
            "Remision not found for Id: {id}".format(id=id),
        )


def read_all(q=""):
    """
    This function responds to a request for /api/people
    with the complete lists of people

    :return:        json string of list of people
    """

    # Create the list of instances from our data
    all_instances = servicio_remision.fetch(q)

    # Serialize the data for the response
    return serialize_many(all_instances)


def actualizar_bitacora(idremision, descripcion):

    # Get the instance requested
    existing_instance = servicio_remision.read_one(idremision)

    if existing_instance is not None:
        
        servicio_remision.actualizar_bitacora(existing_instance, descripcion)

        # Serialize the data for the response
        return serialize_one(existing_instance)
    
    # Otherwise, nope, didn't find that person
    else:
        abort(
            404,
            "Remision not found for Id: {id}".format(id=idremision),
        )


"""
def create(object_data):
    ""
    This function creates a new person in the people structure
    based on the passed in person data

    :param person:  person to create in people structure
    :return:        201 on success, 406 on person exists
    ""


    codigo = object_data.get("codigo")
    estado = object_data.get("estado")
    servicio_referente = object_data.get("servicio_referente")
    servicio_referencia = object_data.get("servicio_referencia")
    info_clinica = object_data.get("info_clinica")

    existing_instance = (
        Remision.query.filter(Remision.codigo == codigo)
        .one_or_none()
    )

    # Can we insert this person?
    if existing_instance is None:

        # Create a person instance using the schema and the passed in person
        schema = PersonaSchema()
        new_instance = schema.make_instance(person)

        # Add the person to the database
        db.session.add(new_instance)
        db.session.commit()

        # Serialize and return the newly created person in the response
        data = schema.dump(new_instance).data

        return data, 201

    # Otherwise, nope, person exists already
    else:
        abort(
            409,
            "La Persona {nombre1} {nombre2} exists already".format(
                nombre1=nombre1, nombre2=nombre2
            ),
        )
"""


def create_fake():
    """
    This function creates a new person in the people structure
    based on the passed in person data

    :param person:  person to create in people structure
    :return:        201 on success, 406 on person exists
    """

    kw = dict()
    kw["fechahora"] = datetime.datetime.now()
    kw["codigo"] = fake.fake_codigo()
    kw["estado"] = RemisionEstado.SOLICITADA
    kw["servicio_referente"] = fake.fake_servicio_referente()
    kw["servicio_referencia"] = fake.servicio_referencia()
    kw["info_clinica"] = fake.fake_info_clinica()
    kw["prestador"] = fake.fake_prestador()
    kw["paciente"] = fake.fake_paciente()
    kw["acudiente"] = fake.fake_acudiente()
    kw["pagador"] = fake.fake_pagador()
    kw["profesional"] = fake.fake_profesional()

    
    # Create a person instance using the schema and the passed in person
    new_instance = Remision(**kw)

    # Add the person to the database
    servicio_remision.solicitar(new_instance)
    
    # Serialize and return the newly created person in the response
    return serialize_one(new_instance)


"""
def update(id, object_data):
    ""
    This function updates an existing person in the people structure

    :param person_id:   Id of the person to update in the people structure
    :param person:      person to update
    :return:            updated person structure
    ""

    # Get the person requested from the db into session
    existing_instance = Remision.query.filter(
        Remision.id == id
    ).one_or_none()

    # Did we find a person?
    if existing_instance is not None:

        # turn the passed in person into a db object
        schema = RemisionSchema()
        update = schema.make_instance(object_data)

        # Set the id to the person we want to update
        update.id = existing_instance.id

        # merge the new object into the old and commit it to the db
        db.session.merge(update)
        db.session.commit()

        # return updated person in the response
        data = schema.dump(existing_instance).data

        return data, 200

    # Otherwise, nope, didn't find that person
    else:
        abort(
            404,
            "Persona not found for Id: {id}".format(id=id),
        )
"""


def delete(id):
    """
    This function deletes a person from the people structure

    :param person_id:   Id of the person to delete
    :return:            200 on successful delete, 404 if not found
    """

    # Get the person requested
    existing_instance = Remision.query.filter(Remision.id == id).one_or_none()

    # Did we find a person?
    if existing_instance is not None:

        db.session.delete(existing_instance)
        db.session.commit()
        return make_response(
            "Remision {id} deleted".format(id=id), 200
        )

    # Otherwise, nope, didn't find that person
    else:
        abort(
            404,
            "Remision not found for Id: {id}".format(id=id),
        )


def load_paciente(**kwargs):
    kwpersona = kwargs["persona"]
    kwargs["persona"] = load_persona(**kwpersona)
    kwargs["direccion"] = kwargs.get("direccion", "")
    kwargs["telefono"] = kwargs.get("telefono", "")

    return PacienteInfo(**kwargs)


def load_acudiente(**kwargs):
    kwpersona = kwargs["persona"]
    kwargs["persona"] = load_persona(**kwpersona)
    kwargs["direccion"] = kwargs.get("direccion", "")
    kwargs["telefono"] = kwargs.get("telefono", "")

    return AcudienteInfo(**kwargs)


def load_profesional(**kwargs):
    kwpersona = kwargs["persona"]
    kwargs["persona"] = load_persona(**kwpersona)
    kwargs["telefono"] = kwargs.get("telefono", "")

    return ProfesionalSaludInfo(**kwargs)


def load_remision(**kwremision):

    kwprestador = kwremision["prestador"]
    kwpagador = kwremision["pagador"]
    kwpaciente = kwremision["paciente"]
    kwacudiente = kwremision["acudiente"]
    kwprofesional = kwremision["profesional"]

    prestador = load_prestador(**kwprestador)
    pagador = load_pagador(**kwpagador)
    paciente = load_paciente(**kwpaciente)
    acudiente = load_acudiente(**kwacudiente)
    profesional = load_profesional(**kwprofesional)

    kw = dict()
    kw["fechahora"] = datetime.datetime.now()
    kw["codigo"] = fake.fake_codigo()
    kw["estado"] = RemisionEstado.SOLICITADA
    kw["servicio_referente"] = kwremision["servicio_referente"]
    kw["servicio_referencia"] = kwremision["servicio_referencia"]
    kw["info_clinica"] = kwremision["info_clinica"]
    kw["prestador"] = prestador
    kw["paciente"] = paciente
    kw["acudiente"] = acudiente
    kw["pagador"] = pagador
    kw["profesional"] = profesional

    return Remision(**kw)


def solicitar(kwremision):
    """
    This function creates a new person in the people structure
    based on the passed in person data

    :param person:  person to create in people structure
    :return:        201 on success, 406 on person exists
    """

    # creacion modelo de remision
    new_instance = load_remision(**kwremision)

    # Add the person to the database
    servicio_remision.solicitar(new_instance)
    
    # Serialize and return the newly created person in the response
    return serialize_one(new_instance)


def recepcionar(id):

    # Get the instance requested
    existing_instance = servicio_remision.read_one(id)

    if existing_instance is not None:
        
        servicio_remision.recepcionar(existing_instance)

        # Serialize the data for the response
        return serialize_one(existing_instance)
    
    # Otherwise, nope, didn't find that person
    else:
        abort(
            404,
            "Remision not found for Id: {id}".format(id=id),
        )


def presentar(idremision, idprestadores):

    # Get the instance requested
    existing_instance = servicio_remision.read_one(idremision)

    if existing_instance is not None:

        prestadores = servicio_prestador.seleccionar_porid(idprestadores)
        
        servicio_remision.presentar(existing_instance, prestadores)

        # Serialize the data for the response
        return serialize_one(existing_instance)
    
    # Otherwise, nope, didn't find that person
    else:
        abort(
            404,
            "Remision not found for Id: {id}".format(id=idremision),
        )


def aceptar(idremision, idprestador):

    # consulta la remision
    remision = servicio_remision.read_one(idremision)
    if remision is None:
        abort(404, "Remision not found for Id: {id}".format(id=idremision))
    
    # consulta el prestador de salud
    prestador = servicio_prestador.read_one(idprestador)
    if prestador is None:
        abort(404, "Prestador de Salud not found for Id: {id}".format(id=idprestador))

    # marca la remision como aceptada por el prestador
    servicio_remision.aceptar(remision, prestador)

    # Serialize the data for the response
    return serialize_one(remision)


def recibir(idremision, idprestador):
    
    # consulta la remision
    remision = servicio_remision.read_one(idremision)
    if remision is None:
        abort(404, "Remision not found for Id: {id}".format(id=idremision))
    
    # consulta el prestador de salud
    prestador = servicio_prestador.read_one(idprestador)
    if prestador is None:
        abort(404, "Prestador de Salud not found for Id: {id}".format(id=idprestador))

    # marca la remision como recibida por el prestador
    servicio_remision.recibir(remision, prestador)

    # Serialize the data for the response
    return serialize_one(remision)


# TODO: Eliminar metodo
def presentar_id(id):

    # Get the instance requested
    existing_instance = servicio_remision.read_one(id)

    if existing_instance is not None:

        prestadores = PrestadorSalud.query \
            .filter((PrestadorSalud.id == 280) | (PrestadorSalud.id == 295)) \
            .all()
        
        servicio_remision.presentar(existing_instance, prestadores)

        # Serialize the data for the response
        return serialize_one(existing_instance)
    
    # Otherwise, nope, didn't find that person
    else:
        abort(
            404,
            "Remision not found for Id: {id}".format(id=id),
        )


# TODO: Eliminar metodo
def aceptar_id(id):

    # Get the instance requested
    existing_instance = servicio_remision.read_one(id)

    if existing_instance is not None:

        prestador = fake.fake_prestador()
        servicio_remision.aceptar(existing_instance, prestador)

        # Serialize the data for the response
        return serialize_one(existing_instance)
    
    else:
        abort(
            404,
            "Remision not found for Id: {id}".format(id=id),
        )


# TODO: Eliminar metodo
def recibir_id(id):
    
    # Get the instance requested
    existing_instance = servicio_remision.read_one(id)
    
    if existing_instance is not None:

        prestador = fake.fake_prestador()
        servicio_remision.recibir(existing_instance, prestador)

        # Serialize the data for the response
        return serialize_one(existing_instance)
    
    else:
        abort(
            404,
            "Remision not found for Id: {id}".format(id=id),
        )
