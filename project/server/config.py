
import os
basedir = os.path.abspath(os.path.dirname(os.path.dirname(__file__)))
database_uri_base = 'sqlite:///' + basedir + '/'
database_name = 'proyecto.db'
database_name_test = 'proyecto_test.db'


class BaseConfig:
    """Base configuration."""
    DEBUG = False
    BCRYPT_LOG_ROUNDS = 13
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SQLALCHEMY_ENGINE_OPTIONS = {
        "pool_recycle": 1800
    }
    SECRET_KEY = os.getenv('SECRET_KEY', 'my_precious')

    # Flask-Mail configuration
    MAIL_SERVER = os.getenv('MAIL_SERVER', None)
    MAIL_PORT = os.getenv('MAIL_PORT', None)
    MAIL_USE_TLS = (os.getenv('MAIL_USE_TLS', False)) == "True"
    MAIL_USE_SSL = (os.getenv('MAIL_USE_SSL', False)) == "True"
    MAIL_DEBUG = (os.getenv('MAIL_DEBUG', False)) == "True"
    MAIL_USERNAME = os.getenv('MAIL_USERNAME', None)
    MAIL_PASSWORD = os.getenv('MAIL_PASSWORD', None)
    MAIL_DEFAULT_SENDER = os.getenv('MAIL_DEFAULT_SENDER', None)


class DevelopmentConfig(BaseConfig):
    """Development configuration."""
    DEBUG = True
    BCRYPT_LOG_ROUNDS = 4
    SQLALCHEMY_DATABASE_URI = os.getenv('SQLALCHEMY_DATABASE_URI', None)


class TestingConfig(BaseConfig):
    """Testing configuration."""
    DEBUG = True
    TESTING = True
    BCRYPT_LOG_ROUNDS = 4
    SQLALCHEMY_DATABASE_URI = os.getenv('SQLALCHEMY_DATABASE_TEST_URI', None)
    PRESERVE_CONTEXT_ON_EXCEPTION = False


class HerokuConfig(BaseConfig):
    """Heroku configuration."""
    DEBUG = True
    BCRYPT_LOG_ROUNDS = 4
    SQLALCHEMY_DATABASE_URI = os.getenv('SQLALCHEMY_DATABASE_URI', None)


class ProductionConfig(BaseConfig):
    """Production configuration."""
    DEBUG = False
    SQLALCHEMY_DATABASE_URI = database_uri_base
