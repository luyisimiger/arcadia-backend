#!/usr/bin/python
# -*- coding: utf-8 -*-

from enum import Enum, unique
from sqlalchemy import ForeignKey
from sqlalchemy.orm import relationship, column_property

from project.server.data.entidades import PrestadorSalud, Pagador
from project.server.data.helpers import enum_values
from project.server.data.user import User
from project.server.extensions import db
from .Persona import Persona
from .RemisionEstado import RemisionEstado
# from .helpers import enum_values


"""
from .Municipio import Municipio
from .EntidadSalud import EntidadSalud
from .Pagador import Pagador
from .PrestadorSalud import PrestadorSalud
"""


class PacienteInfo(db.Model):

    __tablename__ = 'remisiones_paciente'

    id = db.Column(db.Integer, primary_key=True)
    idpersona = db.Column(db.Integer, ForeignKey(Persona.id))

    direccion = db.Column(db.String(256), nullable=False)
    telefono = db.Column(db.String(100), nullable=False)

    persona = relationship(Persona)

    def __repr__(self):
        return '<Paciente: %s %s - %s %s>' % (self.persona.tipoIdentificacion, self.persona.numeroIdentificacion, self.persona.nombre1, self.persona.apellido1)

class AcudienteInfo(db.Model):

    __tablename__ = 'remisiones_acudiente'

    id = db.Column(db.Integer, primary_key=True)
    idpersona = db.Column(db.Integer, ForeignKey(Persona.id))

    direccion = db.Column(db.String(256), nullable=False)
    telefono = db.Column(db.String(100), nullable=False)

    persona = relationship(Persona)

    def __repr__(self):
        return '<ResponsablePaciente: %s %s - %s %s>' % (self.persona.tipoIdentificacion, self.persona.numeroIdentificacion, self.persona.nombre1, self.persona.apellido1)

    @property
    def nombrecompleto(self):
        return self.persona.nombrecompleto()


class ProfesionalSaludInfo(db.Model):

    __tablename__ = 'remisiones_profesional_salud'

    id = column_property(db.Column(db.Integer, primary_key=True), Persona.id)
    idpersona = db.Column(db.Integer, ForeignKey(Persona.id))

    telefono = db.Column(db.String(100), nullable=False)

    persona = relationship(Persona)


    def __repr__(self):
        return '<ProfesionalSalud: %s %s - %s %s>' % (self.persona.tipoIdentificacion, self.persona.numeroIdentificacion, self.persona.nombre1, self.persona.apellido1)
    
    @property
    def nombrecompleto(self):
        return self.persona.nombrecompleto()


class Remision(db.Model):

    __tablename__ = 'remisiones'

    id = db.Column(db.Integer, primary_key=True)
    fechahora = db.Column(db.DateTime, nullable=False)
    codigo = db.Column(db.String(50), unique=True, nullable=False)
    estado = db.Column(db.Enum(RemisionEstado, values_callable=enum_values, native_enum=False), nullable=False)
    servicio_referente = db.Column(db.String(100), nullable=False)
    servicio_referencia = db.Column(db.String(100), nullable=False)
    info_clinica = db.Column(db.String(100), nullable=False)

    idprestador = db.Column(db.Integer, ForeignKey(PrestadorSalud.id))
    idpaciente = db.Column(db.Integer, ForeignKey(PacienteInfo.id))
    idacudiente = db.Column(db.Integer, ForeignKey(AcudienteInfo.id))
    idpagador = db.Column(db.Integer, ForeignKey(Pagador.id))
    idprofesional = db.Column(db.Integer, ForeignKey(ProfesionalSaludInfo.id))
    
    prestador = relationship(PrestadorSalud)
    paciente = relationship(PacienteInfo)
    acudiente = relationship(AcudienteInfo, foreign_keys=[idacudiente])
    pagador = relationship(Pagador)
    profesional = relationship(ProfesionalSaludInfo, foreign_keys=[idprofesional])
    bitacora = relationship("RemisionEvento")

    def __repr__(self):
        return '<Remision: %s - %s>' % (self.id, self.codigo)


class RemisionEvento(db.Model):

    __tablename__ = 'remisiones_evento'

    id = db.Column(db.Integer, primary_key=True)
    tipo = db.Column(db.Enum(RemisionEstado, values_callable=enum_values, native_enum=False), nullable=False)
    fechahora = db.Column(db.DateTime, nullable=False)
    descripcion = db.Column(db.String(250), nullable=False)

    idremision = db.Column(db.Integer, ForeignKey(Remision.id))
    idusuario = db.Column(db.Integer, ForeignKey(User.id), nullable=True)

    remision = relationship(Remision)

    def __repr__(self):
        return '<RemisionEvento: %s>' % (self.descripcion)


class RemisionPresentada(db.Model):

    __tablename__ = 'remisiones_presentadas'

    id = db.Column(db.Integer, primary_key=True)
    idevento = db.Column(db.Integer, ForeignKey(RemisionEvento.id))
    idremision = db.Column(db.Integer, ForeignKey(Remision.id))
    idprestador = db.Column(db.Integer, ForeignKey(PrestadorSalud.id))
    respuesta = db.Column(db.Enum(RemisionEstado, values_callable=enum_values, native_enum=False), nullable=False, default=RemisionEstado.PRESENTADA)
    
    evento = relationship(RemisionEvento)
    remision = relationship(Remision)
    prestador = relationship(PrestadorSalud)

    def __repr__(self):
        return '<RemisionPresentada: %s %s>' % (self.remision.codigo, self.prestador.nombre)
