
from project.server.extensions import db


class Persona(db.Model):

    __tablename__ = 'personas'

    id = db.Column(db.Integer, primary_key=True)
    apellido1 = db.Column(db.String(100), nullable=False)
    apellido2 = db.Column(db.String(100), nullable=False)
    nombre1 = db.Column(db.String(100), nullable=False)
    nombre2 = db.Column(db.String(100), nullable=False)
    tipoIdentificacion = db.Column(db.String(2), nullable=False)
    numeroIdentificacion = db.Column(db.String(100), nullable=False)
    fechaNacimiento = db.Column(db.Date, nullable=False)
    sexo = db.Column(db.String(1), nullable=False)

    def to_dict(self):
        return dict(id=self.id,
                    apellido1=self.apellido1,
                    apellido2=self.apellido2,
                    nombre1=self.nombre1,
                    nombre2=self.nombre2,
                    tipoIdentificacion=self.tipoIdentificacion,
                    numeroIdentificacion=self.numeroIdentificacion,
                    fechaNacimiento=self.fechaNacimiento,
                    sexo=self.sexo)

    def nombrecompleto(self):
        return '%s %s %s %s' % (self.nombre1, self.nombre2,
                                self.apellido1, self.apellido2)

    def __repr__(self):
        return '<Persona: %s %s - %s>' % (self.tipoIdentificacion,
                                          self.numeroIdentificacion,
                                          self.nombrecompleto())
