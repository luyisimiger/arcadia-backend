#!/usr/bin/python
# -*- coding: utf-8 -*-

from enum import Enum, unique


@unique
class RemisionEstado(Enum):
    SOLICITADA = 1
    DEVUELTA = 2
    PENDIENTE = 3
    PRESENTADA = 4
    ACEPTADA = 5
    SUSPENDIDA = 6
    RECIBIDA = 7
