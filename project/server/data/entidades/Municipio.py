
from sqlalchemy import ForeignKey
from sqlalchemy.orm import relationship

from project.server.extensions import db


class Municipio(db.Model):

    __tablename__ = 'municipios'

    id = db.Column(db.Integer, primary_key=True)
    codigo = db.Column(db.String(10), unique=True, nullable=False)
    nombre = db.Column(db.String(100), nullable=False)
    latitud = db.Column(db.Float, nullable=False)
    longitud = db.Column(db.Float, nullable=False)
    iddepartamento = db.Column(db.Integer, ForeignKey('departamentos.id'))

    departamento = relationship("Departamento")

    def to_dict(self):
        return dict(id=self.id,
                    codigo=self.codigo,
                    nombre=self.nombre,
                    latitud=self.latitud,
                    longitud=self.longitud,
                    departamento=self.departamento.to_dict())

    def __repr__(self):
        return '<Municipio: %s - %s>' % (self.codigo, self.nombre)