#!/usr/bin/python
# -*- coding: utf-8 -*-

from enum import Enum, unique


@unique
class TipoEntidadSalud(Enum):
    PAGADOR = 1
    PRESTADOR = 2


@unique
class TipoPagadorSalud(Enum):
    EPS = 1
    ARL = 2
    ENTIDAD_ADAPTADA = 3
    DIR_TERRITORIAL_SALUD = 4


@unique
class TipoPrestadorSalud(Enum):
    IPS = 1
    PROFESIONAL_INDEPENDIENTE = 2
    TRANSPORTE_ESPECIAL = 3


from .Departamento import Departamento
from .Municipio import Municipio
from .EntidadSalud import EntidadSalud
from .Pagador import Pagador
from .PrestadorSalud import PrestadorSalud
