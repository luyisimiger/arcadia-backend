from project.server.extensions import db
from . import TipoEntidadSalud


class EntidadSalud(db.Model):

    __tablename__ = 'entidadessalud'

    id = db.Column(db.Integer, primary_key=True)
    clase = db.Column(db.Enum(TipoEntidadSalud), nullable=False, )
    codigo = db.Column(db.String(50), unique=True, nullable=False)
    sigla = db.Column(db.String(500), nullable=False)
    nombre = db.Column(db.String(500), nullable=False)
    direccion = db.Column(db.String(500), nullable=False)
    telefono = db.Column(db.String(500), nullable=False)
    url = db.Column(db.String(500), nullable=False)
    correo_referencia = db.Column(db.String(100), nullable=True)

    __mapper_args__ = {'polymorphic_on': clase}

    def to_dict(self):
        return dict(id=self.id,
                    codigo=self.codigo,
                    clase=self.clase,
                    sigla=self.sigla,
                    nombre=self.nombre,
                    direccion=self.direccion,
                    telefono=self.telefono,
                    url=self.url)

    def __repr__(self):
        return '<EntidadSalud: %s - %s>' % (self.codigo, self.sigla)
