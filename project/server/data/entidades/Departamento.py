
from project.server.extensions import db


class Departamento(db.Model):

    __tablename__ = 'departamentos'

    id = db.Column(db.Integer, primary_key=True)
    codigo = db.Column(db.String(2), unique=True, nullable=False)
    nombre = db.Column(db.String(100), nullable=False)
    latitud = db.Column(db.Float, nullable=False)
    longitud = db.Column(db.Float, nullable=False)

    def to_dict(self):
        return dict(id=self.id,
                    codigo=self.codigo,
                    nombre=self.nombre,
                    latitud=self.latitud,
                    longitud=self.longitud)

    def __repr__(self):
        return '<Departamento: %s - %s>' % (self.codigo, self.nombre)
