from sqlalchemy import ForeignKey
from sqlalchemy.orm import relationship

from project.server.extensions import db

from . import Municipio
from . import EntidadSalud
from . import TipoEntidadSalud, TipoPrestadorSalud


class PrestadorSalud(EntidadSalud):

    __tablename__ = 'prestadores'
    __mapper_args__ = {'polymorphic_identity': TipoEntidadSalud.PRESTADOR}

    id = db.Column(db.Integer, ForeignKey(EntidadSalud.id), primary_key=True)
    tipo = db.Column(db.Enum(TipoPrestadorSalud), nullable=False)
    idmunicipio = db.Column(db.Integer, ForeignKey(Municipio.id))

    municipio = relationship(Municipio)

    def to_dict(self):
        d = super(PrestadorSalud, self).to_dict()
        d["tipo"] = self.tipo
        # d["municipio"] = self.municipio.to_dict()
        return d

    def __repr__(self):
        return '<PrestadorSalud: %s - %s>' % (self.codigo, self.nombre)
