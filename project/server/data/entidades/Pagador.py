from sqlalchemy import ForeignKey
from project.server.extensions import db

from . import EntidadSalud
from . import TipoEntidadSalud, TipoPagadorSalud


class Pagador(EntidadSalud):

    __tablename__ = 'pagadores'
    __mapper_args__ = {'polymorphic_identity': TipoEntidadSalud.PAGADOR}

    id = db.Column(db.Integer, ForeignKey(EntidadSalud.id), primary_key=True)
    tipo = db.Column(db.Enum(TipoPagadorSalud), nullable=False)

    def to_dict(self):
        d = super(Pagador, self).to_dict()
        d["tipo"] = self.tipo
        return d

    def __repr__(self):
        return '<Pagador: %s - %s>' % (self.codigo, self.nombre)
