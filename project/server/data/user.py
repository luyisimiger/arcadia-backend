import datetime
from werkzeug.security import generate_password_hash, check_password_hash

from ..application import app
from ..extensions import db, bcrypt


class User(db.Model):

    __tablename__ = 'usuarios'

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    email = db.Column(db.String(255), unique=True, nullable=False)
    password = db.Column(db.String(255), nullable=False)
    registered_on = db.Column(db.DateTime, nullable=False)
    admin = db.Column(db.Boolean, nullable=False, default=False)

    def __init__(self, email, password, admin=False):
        self.email = email
        self.password = bcrypt.generate_password_hash(
            password, app.config.get('BCRYPT_LOG_ROUNDS')
        ).decode()
        self.registered_on = datetime.datetime.now()
        self.admin = admin

    @classmethod
    def authenticate(cls, **kwargs):
        email = kwargs.get('email')
        password = kwargs.get('password')
        if not (email and password):
            return
            user = (cls.query.filter_by(email=email)).first()
            if not (user and check_password_hash(user.password, password)):
                return
                return user

    def to_dict(self):
        return dict(id=self.id, email=self.email)
