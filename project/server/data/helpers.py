
def enum_values(enum_cls):
    members = enum_cls.__members__    
    return [str(i.value) for i in members.values()]
