from marshmallow import post_dump
from marshmallow.fields import Field
from marshmallow_sqlalchemy import SQLAlchemyAutoSchema, SQLAlchemyAutoSchemaOpts
from marshmallow_sqlalchemy.convert import ModelConverter
from marshmallow_sqlalchemy.fields import Nested
from sqlalchemy import Enum

from .extensions import db
from project.server.data import (
    Departamento,
    Municipio,
    Pagador,
    PrestadorSalud,
    Persona,
    PacienteInfo,
    AcudienteInfo,
    ProfesionalSaludInfo,
    Remision,
    RemisionEvento
)


class EnumField(Field):
    def __init__(self, *args, **kwargs):
        self.column = kwargs.get('column')
        super(EnumField, self).__init__(*args, **kwargs)

    def _serialize(self, value, attr, obj):
        if value is None:
            return None
        else:
            return value.value

    def deserialize(self, value, attr=None, data=None):
        field = super(EnumField, self).deserialize(value, attr, data)
        if isinstance(field, str) and self.column is not None:
            return self.column.type.python_type[field]
        return field


class ExtendModelConverter(ModelConverter):
    SQLA_TYPE_MAPPING = {
        **ModelConverter.SQLA_TYPE_MAPPING,
        Enum: EnumField,
    }

    def _add_column_kwargs(self, kwargs, column):
        super()._add_column_kwargs(kwargs, column)
        if hasattr(column.type, 'enums'):
            kwargs['column'] = column


class MySQLAlchemyAutoSchemaOpts(SQLAlchemyAutoSchemaOpts):
    def __init__(self, meta, *args, **kwargs):
        # setattr(meta, "sqla_session", db.session)
        
        # set default dateformat
        dateformat = getattr(meta, "dateformat", None)
        if dateformat is None:
            setattr(meta, "dateformat", "%Y-%m-%d")

        # set default datetimeformat
        datetimeformat = getattr(meta, "datetimeformat", None)
        if datetimeformat is None:
            setattr(meta, "datetimeformat", "%Y-%m-%dT%H:%M:%S.%f-05:00")
        
        # init super class
        super().__init__(meta, *args, **kwargs)


class MyModelSchema(SQLAlchemyAutoSchema):
    OPTIONS_CLASS = MySQLAlchemyAutoSchemaOpts


class DepartamentoSchema(MyModelSchema):
    class Meta:
        model = Departamento


class MunicipioSchema(MyModelSchema):

    departamento = Nested(DepartamentoSchema)

    class Meta:
        model = Municipio


class PagadorSchema(MyModelSchema):
    class Meta:
        model = Pagador
        model_converter = ExtendModelConverter


class PrestadorSaludSchema(MyModelSchema):

    municipio = Nested(MunicipioSchema)

    class Meta:
        model = PrestadorSalud
        model_converter = ExtendModelConverter


class PersonaSchema(MyModelSchema):
    class Meta:
        model = Persona


class PacienteInfoSchema(MyModelSchema):

    persona = Nested(PersonaSchema)

    class Meta:
        model = PacienteInfo


class AcudienteInfoSchema(MyModelSchema):

    persona = Nested(PersonaSchema)

    class Meta:
        model = AcudienteInfo


class ProfesionalSaludInfoSchema(MyModelSchema):

    persona = Nested(PersonaSchema)

    class Meta:
        model = AcudienteInfo


class RemisionEventoSchema(MyModelSchema):
    class Meta:
        model = RemisionEvento
        model_converter = ExtendModelConverter


class RemisionSchema(MyModelSchema):

    prestador = Nested(PrestadorSaludSchema)
    paciente = Nested(PacienteInfoSchema)
    acudiente = Nested(AcudienteInfoSchema)
    pagador = Nested(PagadorSchema)
    profesional = Nested(ProfesionalSaludInfoSchema)
    bitacora = Nested(RemisionEventoSchema, many=True)

    class Meta:
        model = Remision
        model_converter = ExtendModelConverter
