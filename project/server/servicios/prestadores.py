from project.server.data import PrestadorSalud, Municipio, RemisionPresentada, RemisionEstado
from project.server.servicios import remisiones as servicio_remision

def read_one(id):

    # obtener prestador
    existing_instance = PrestadorSalud.query.filter(PrestadorSalud.id == id).one_or_none()
    return existing_instance


def seleccionar_porid(ids):

    # obtener lista de prestadores por id
    all_instances = PrestadorSalud.query \
        .filter(PrestadorSalud.id.in_(ids)) \
        .all()
    
    return all_instances


def seleccionar_porcodigo(codigos):

    # obtener lista de prestadores por codigo
    all_instances = PrestadorSalud.query \
        .filter(PrestadorSalud.codigo.in_(codigos)) \
        .all()
    
    return all_instances


def seleccionar(**kwargs):

    # obtener filtros
    query = kwargs.get('query', "")
    idmunicipios = kwargs.get('idmunicipios', [])
    idremision = kwargs.get('idremision', 0)
    xaction = kwargs.get('xaction', "")

    # filtrar por prestador de salud
    saquery = PrestadorSalud.query.join(PrestadorSalud.municipio)
    
    # hay remision relacionada    
    if xaction in ("ACEPTAR", "RECIBIR"):

        remision = servicio_remision.read_one(idremision)
        xrespuesta = None

        if xaction == "ACEPTAR":
            xrespuesta = RemisionEstado.PRESENTADA
        elif xaction == "RECIBIR":
            xrespuesta = RemisionEstado.ACEPTADA

        if remision is not None:
            saquery = saquery.join(RemisionPresentada) \
                .filter(RemisionPresentada.idremision == remision.id) \
                .filter(RemisionPresentada.respuesta == str(xrespuesta.value))
    
    
    # filtrar por municipio
    if len(idmunicipios):
        saquery = saquery.filter(Municipio.id.in_(idmunicipios))
    
    # filtrar por nombre
    if query is not None:
        saquery = saquery.filter(PrestadorSalud.nombre.ilike("%" + query + "%"))
    
    print(saquery)

    # obtener lista de prestadores
    all_instances = saquery \
        .order_by(PrestadorSalud.codigo) \
        .all()
    
    return all_instances
