from flask_mail import Message
from project.server.application import app
from project.server.extensions import mail
from project.server.decorators import asincrono


@asincrono
def send_async_email(app, msg):
    with app.app_context():
        mail.send(msg)

def send_email(subject, sender, recipients, text_body, html_body):
    msg = Message(subject, sender=sender, recipients=recipients)
    msg.body = text_body
    msg.html = html_body
    send_async_email(app, msg)
