from datetime import datetime
from flask_mail import Message
from flask import render_template
import lorem

from project.server.application import app
from project.server.extensions import db, mail
from project.server.data import Remision, RemisionEstado, RemisionEvento, RemisionPresentada
from project.server.servicios.emails import send_email


def read_one(id):

    # Get the instance requested
    existing_instance = Remision.query.filter(Remision.id == id).one_or_none()
    return existing_instance


def fetch(query=""):

    all_instances = Remision.query \
        .order_by(Remision.fechahora) \
        .all()
    
    return all_instances


def fetch_presentadas(prestador):
    
    all_instances = all_instances = RemisionPresentada.query \
        .filter(RemisionPresentada.idprestador == prestador.id) \
        .all()
    
    return all_instances


def fetch_remision_presentada(remision, prestador):
    
    all_instances = all_instances = RemisionPresentada.query \
        .filter(RemisionPresentada.idprestador == prestador.id) \
        .filter(RemisionPresentada.idremision == remision.id) \
        .filter(RemisionPresentada.respuesta == RemisionEstado.PRESENTADA) \
        .all()
    
    return all_instances


def fetch_remision_aceptada(remision, prestador):
    
    all_instances = all_instances = RemisionPresentada.query \
        .filter(RemisionPresentada.idprestador == prestador.id) \
        .filter(RemisionPresentada.idremision == remision.id) \
        .filter(RemisionPresentada.respuesta == RemisionEstado.ACEPTADA) \
        .all()
    
    return all_instances


def actualizarBitacora(remision):
    evento = RemisionEvento(remision=remision)
    evento.fechahora = datetime.now()
    evento.tipo = remision.estado

    return evento


def _actualizar_bitacora(remision, descripcion):
    evento = actualizarBitacora(remision)
    evento.descripcion = descripcion

    db.session.add(evento)
    return evento


def actualizar_bitacora(remision, descripcion):
    evento = _actualizar_bitacora(remision, descripcion)
    db.session.commit()


def registar_respuesta_prestador(remision, prestador, respuesta):

    if respuesta == RemisionEstado.RECIBIDA:
        fn = fetch_remision_aceptada
    else:
        fn = fetch_remision_presentada

    presentaciones = fn(remision, prestador)

    for p in presentaciones:
        p.respuesta = respuesta
        db.session.add(p)
    
    return presentaciones


def solicitar(remision):
    
    if remision is None:
        return
    
    descripcion = "Se solicita la remisión"
    evento = _actualizar_bitacora(remision, descripcion)

    db.session.add(remision)
    db.session.commit()


def recepcionar(remision):
    
    if remision is None:
        return
    
    correo = remision.prestador.correo_referencia
    destinatarios = [correo]

    asunto = render_template("emails/remisiones/recepcionar_asunto.txt", remision=remision)
    emisor = None
    text_body = render_template("emails/remisiones/recepcionar.txt", remision=remision)
    text_html = None
    send_email(asunto, emisor, destinatarios, text_body, text_html)
    
    remision.estado = RemisionEstado.PENDIENTE
    
    descripcion = "Se recepciona la remisión"
    evento = _actualizar_bitacora(remision, descripcion)

    db.session.add(remision)
    db.session.commit()


def presentar(remision, prestadores):
    
    if remision is None:
        return
    
    # actualizar estado de la remision
    remision.estado = RemisionEstado.PRESENTADA
    
    # actualizar bitacora
    descripcion = "Se presenta la remisión a {} prestadores de salud.".format(len(prestadores))
    evento = _actualizar_bitacora(remision, descripcion)

    # presentar remision a cada prestador
    destinatarios = []
    for p in prestadores:
        correo = p.correo_referencia
        print(correo)
        destinatarios.append(correo)
        rp = RemisionPresentada(remision=remision, prestador=p, evento=evento)
        db.session.add(rp)
    
    # enviar correo electronico
    asunto = render_template("emails/remisiones/presentar_asunto.txt", remision=remision)
    emisor = None
    text_body = render_template("emails/remisiones/presentar.txt", remision=remision)
    text_html = None
    send_email(asunto, emisor, destinatarios, text_body, text_html)
    
    # guardar en base de datos
    db.session.add(remision)
    db.session.commit()


def aceptar(remision, prestador):
    
    if remision is None:
        return
    
    correo = remision.prestador.correo_referencia
    destinatarios = [correo]

    # enviar correo electronico
    asunto = render_template("emails/remisiones/aceptar_asunto.txt", remision=remision)
    emisor = None
    text_body = render_template("emails/remisiones/aceptar.txt", remision=remision, prestador=prestador)
    text_html = None
    send_email(asunto, emisor, destinatarios, text_body, text_html)
    
    # actualizar estado de la remision
    remision.estado = RemisionEstado.ACEPTADA
    
    # actualizar bitacora
    descripcion = "El prestador de salud, {}, ha aceptado la remisión".format(prestador.nombre)
    _actualizar_bitacora(remision, descripcion)

    # marcar remision como aceptada por el prestador
    registar_respuesta_prestador(remision, prestador, RemisionEstado.ACEPTADA)
    
    # guardar en base de datos
    db.session.add(remision)
    db.session.commit()


def recibir(remision, prestador):
    
    if remision is None:
        return
    
    correo = remision.prestador.correo_referencia
    destinatarios = [correo]

    # enviar correo electronico
    asunto = render_template("emails/remisiones/recibir_asunto.txt", remision=remision)
    emisor = None
    text_body = render_template("emails/remisiones/recibir.txt", remision=remision, prestador=prestador)
    text_html = None
    send_email(asunto, emisor, destinatarios, text_body, text_html)

    # actualizar estado de la remision
    remision.estado = RemisionEstado.RECIBIDA
    
    # actualizar bitacora
    descripcion = "El prestador de salud, {}, ha recibido la remisión".format(prestador.nombre)
    evento = _actualizar_bitacora(remision, descripcion)
    
    # marcar remision como aceptada por el prestador
    registar_respuesta_prestador(remision, prestador, RemisionEstado.RECIBIDA)

    # guardar en base de datos
    db.session.add(remision)
    db.session.commit()
