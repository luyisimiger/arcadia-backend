from project.server.data import Pagador
from project.server.data.entidades import TipoPagadorSalud


def read_one(id):

    # obtener prestador
    existing_instance = Pagador.query.filter(Pagador.id == id).one_or_none()
    return existing_instance


def seleccionar(**kwargs):

    # obtener filtros
    query = kwargs.get('query', "")
    tipo_pagador_list = kwargs.get("tipo_pagador_list", [])

    # filtrar por pagador
    saquery = Pagador.query

    # filtrar por municipio
    if len(tipo_pagador_list):
        xtipo = [ TipoPagadorSalud(i) for i in tipo_pagador_list]
        saquery = saquery.filter(Pagador.tipo.in_(xtipo))

    # filtrar por nombre
    if query is not None:
        saquery = saquery.filter(Pagador.nombre.ilike("%" + query + "%"))

    # obtener lista de prestadores
    all_instances = saquery \
        .order_by(Pagador.codigo) \
        .all()
    
    return all_instances
