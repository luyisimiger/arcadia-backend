
from .application import app
from .extensions import db, bcrypt, ma, mail
from .auth.views import auth_blueprint
from .api import api_blueprint


db.init_app(app)
bcrypt.init_app(app)
ma.init_app(app)
mail.init_app(app)


app.register_blueprint(auth_blueprint)
app.register_blueprint(api_blueprint)


__all__ = ["app", "db"]
