# project/tests/test_user_model.py

import unittest

from project.server.extensions import db
from project.server.auth import encode_auth_token, decode_auth_token
from project.server.models import User
from project.tests.base import BaseTestCase


class TestUserModel(BaseTestCase):

    def add_user(self):
        user = User(
            email='test@test.com',
            password='test'
        )
        db.session.add(user)
        db.session.commit()
        return user

    def test_encode_auth_token(self):
        user = self.add_user()
        auth_token = encode_auth_token(user)
        self.assertTrue(isinstance(auth_token, bytes))
    
    def test_decode_auth_token(self):
        user = self.add_user()
        auth_token = encode_auth_token(user)
        self.assertTrue(isinstance(auth_token, bytes))
        self.assertTrue(decode_auth_token(auth_token.decode("utf-8")) == 1)


if __name__ == '__main__':
    unittest.main()
