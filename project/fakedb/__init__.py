
from flask_script import Manager

from .fake import fakedb

manager = Manager(usage='Create json server file fake database')

# __all__ = ["fakedb"]


@manager.command
def db():
    fakedb()
