import datetime
import json
import random

from os import path

JSON_ENTIDADESDB_FILE = path.abspath(path.join(
    path.dirname(path.dirname(__file__)), "entidades/entidades.json"))

JSON_DB_FILE = path.abspath(path.join(path.dirname(__file__), "db.json"))

idreferencia = 0

jsfile = open(JSON_ENTIDADESDB_FILE, mode="r", encoding='utf8')

entidadesdb = json.load(jsfile)


def get_fake_fecha():

    now = datetime.datetime.now()
    days = random.randint(1, 60)
    hours = random.randint(1, 23)
    minutes = random.randint(1, 59)
    seconds = random.randint(1, 59)

    delta = datetime.timedelta(days=days, hours=hours,
                               minutes=minutes, seconds=seconds)

    return now - delta


def get_fake_estado():

    estados = ["Solicitada", "Pendiente", "Prestada", "Aceptada", "Recibida"]
    estado = random.choice(estados)
    return estado


def get_fake_entity(entityname):
    return random.choice(entidadesdb[entityname])


def get_fake_prestador():
    return get_fake_entity("prestadores")


def get_fake_persona():
    return get_fake_entity("personas")


def get_fake_pagador():
    return get_fake_entity("pagadores")


def get_fake_referencia():

    referencia = {
        "id": idreferencia,
        "fecha": str(get_fake_fecha()),
        "estado": get_fake_estado(),
        "paciente": get_fake_persona(),
        "acudiente": get_fake_persona(),
        "profesional": get_fake_persona(),
        "prestador": get_fake_prestador(),
        "pagador": get_fake_pagador(),
    }

    return referencia


def fakedb():

    total = random.randint(1, 220)

    referencias = []
    for i in range(total):
        referencias.append(get_fake_referencia())

    database = dict()
    database["referencias"] = referencias

    jsdbfile = open(JSON_DB_FILE, mode="w", encoding='utf8')

    json.dump(database, jsdbfile, indent=4)
    jsdbfile.close

    jsfile.close
