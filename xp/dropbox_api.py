import os
from dropbox import Dropbox
from dotenv import load_dotenv

load_dotenv()

acess_token = os.getenv('DROPBOX_ACCESS_TOKEN', None)
dbx = Dropbox(acess_token)

for entry in dbx.files_list_folder('').entries:
    print(entry.name)

fullname = input("upload file: ")
print (fullname)

name = "/%s" % os.path.basename(fullname)

with open(fullname, 'rb') as f:
    response = dbx.files_upload(f.read(), name)

    file_link_metadata = dbx.sharing_create_shared_link_with_settings(response.path_lower)
    print(file_link_metadata.url)
