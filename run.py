"""
run.py
- creates an application instance and runs the dev server
"""
import env

from project.server.application import app

if __name__ == '__main__':
    app.run(host='0.0.0.0')
